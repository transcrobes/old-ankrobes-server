import configparser
import logging
import os
from os.path import dirname, realpath

paths = [
    "/etc/ankisyncd/ankisyncd.conf",
    os.environ.get("XDG_CONFIG_HOME") and
        (os.path.join(os.environ['XDG_CONFIG_HOME'], "ankisyncd", "ankisyncd.conf")) or
        os.path.join(os.path.expanduser("~"), ".config", "ankisyncd", "ankisyncd.conf"),
    os.path.join(dirname(dirname(realpath(__file__))), "ankisyncd.conf"),
]


def load(path=None):
    choices = paths
    parser = configparser.ConfigParser()
    if path:
        choices = [path]
    for path in choices:
        logging.debug("config.location: trying", path)
        try:
            parser.read(path)

            # FIXME: Horrible hack to get it more kube-compatible
            if os.getenv('ANKROBES_DATA_ROOT'):
                parser['sync_app']['data_root'] = os.getenv('ANKROBES_DATA_ROOT')
            if os.getenv('ANKROBES_HOST_LISTEN'):
                parser['sync_app']['host'] = os.getenv('ANKROBES_HOST_LISTEN')
            if os.getenv('ANKROBES_HOST_PORT'):
                parser['sync_app']['port'] = os.getenv('ANKROBES_HOST_PORT')
            if os.getenv('ANKROBES_AUTH_URL'):
                parser['sync_app']['auth_url'] = (os.getenv('ANKROBES_AUTH_URL') or "").strip()

            conf = parser['sync_app']
            logging.info("Loaded config from {}".format(path))
            return conf
        except KeyError:
            pass
    raise Exception("No config found, looked for {}".format(", ".join(choices)))
